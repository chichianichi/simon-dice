/*Constantes requeridas para la clase Juego()*/
const orquidea = document.getElementById("orquidea");
const violeta = document.getElementById("violeta");
const rosa = document.getElementById("rosa");
const melocoton = document.getElementById("melocoton");
const btnEmpezar = document.getElementById("btnEmpezar");
const ULTIMO_NIVEL = 15;

/*Constantes que se usan para los sonidos del juego, creados como objetos tipo Audio() */
const sonidoOrquidea = new Audio("sounds/boton1.mp3");
const sonidoVioleta = new Audio("sounds/boton2.mp3");
const sonidoRosa = new Audio("sounds/boton3.mp3");
const sonidoMelocoton = new Audio("sounds/boton4.mp3");

/*Variables usadas para el cronometro del juego */
let tiempo, intervalo = 0;
let verificador = false;

/*Instrucciones() deben ser mostradas desde el momento que carga la página */
instrucciones();

class Juego {
  constructor() {
    this.inicializar = this.inicializar.bind(this)

    this.inicializar();
    this.generarSecuencia();
    setTimeout(this.siguienteNivel(), 800)

  }

  /*Inicializa las variables que se requieren a traves del código del juego */
  inicializar() {
    this.siguienteNivel = this.siguienteNivel.bind(this);
    this.elegirColor = this.elegirColor.bind(this);
    tiempo = 0;
    iniciaContador();
    this.toggleBtnEmpezar()
    this.nivel = 1;
    this.colores = {
      orquidea,
      violeta,
      rosa,
      melocoton
    }
  }
  
  /*habilita o deshabilita el boton Empezar a Jugar */
  toggleBtnEmpezar() {
    if (btnEmpezar.classList.contains("hide")) {
      btnEmpezar.classList.remove("hide");
    } else {
      btnEmpezar.classList.add("hide");
    }
  }

  /*Genera la secuencia con numero aleatorios entre (0,3), todos serán numeros enteros */
  generarSecuencia() {    
    this.secuencia = new Array(ULTIMO_NIVEL).fill(0).map(n => Math.floor(Math.random() * 4));
  }

  /*Cuando empieza un nivel habilita eventos de click e ilumina la secuencia aleatoria del juego*/
  siguienteNivel() {
    this.subnivel = 0;
    this.iluminarSecuencia();
    this.agregarEventosClick();
  }

  /*Recibe un numero que proviene del arreglo de la secuencia y lo traduce a un String 
  0 --> orquide
  1 --> violeta
  2 -> rosa
  3 --> melocoton*/
  transformarNumeroAColor(numero) {
    switch (numero) {
      case 0:
        return "orquidea";
      case 1:
        return "violeta";
      case 2:
        return "rosa";
      case 3:
        return "melocoton";
    }
  }

  /*Recibe un String con el nombre de un color y lo traduce a un numero correspondiente al color
  orquidea --> 0
  violeta --> 1
  rosa --> 2
  melocoton --> */
  transformarColorANumero(color) {
    switch (color) {
      case "orquidea":
        return 0;
      case "violeta":
        return 1;
      case "rosa":
        return 2;
      case "melocoton":
        return 3;
    }
  }

  /*Reproduce un sonido dependiendo del color que recibe */
  eligeSonido(color) {
    switch (color) {
      case "orquidea":
        sonidoOrquidea.play();
        break;
      case "violeta":
        sonidoVioleta.play();
        break;
      case "rosa":
        sonidoRosa.play();
        break;
      case "melocoton":
        sonidoMelocoton.play();
        break;
    }
  }

/*llama a iluminaColor() para que sea visible el patron en el juego */
  iluminarSecuencia() {
    for (let i = 0; i < this.nivel; i++) {
      const color = this.transformarNumeroAColor(this.secuencia[i]);
      setTimeout(() => this.iluminarColor(color), 1000 * i);
    }
  }

  /*ilumina el color con su respectivo sonido, para el color se debe añadir la palabra light y esto
  cambia el nombre de la clase en html */
  iluminarColor(color) {
    this.colores[color].classList.add("light");
    this.eligeSonido(color);

    setTimeout(() => this.apagarColor(color), 350);
  }

  /*Cambia el nombre de la clase html, removiendo la palabra light */
  apagarColor(color) {
    this.colores[color].classList.remove("light");
  }

  /*habilita el evento del click sobre los colores que se ven en pantalla */
  agregarEventosClick() {
    this.colores.orquidea.addEventListener("click", this.elegirColor);
    this.colores.melocoton.addEventListener("click", this.elegirColor);
    this.colores.violeta.addEventListener("click", this.elegirColor);
    this.colores.rosa.addEventListener("click", this.elegirColor);
  }

  /*deshabilita el evento del click sobre los colores que se ven en pantalla */
  eliminarEventosClick() {
    this.colores.orquidea.removeEventListener("click", this.elegirColor);
    this.colores.melocoton.removeEventListener("click", this.elegirColor);
    this.colores.violeta.removeEventListener("click", this.elegirColor);
    this.colores.rosa.removeEventListener("click", this.elegirColor);
  }


/*Cuando seleccionamos un color en el juego, la funcion verifica si estamos siguiendo el patron
y si se completo el patron. Aqui es donde se verifica el patron para saber si ganamos o perdemos el juegos */
  elegirColor(ev) {
    const nombreColor = ev.target.dataset.color;
    const numeroColor = this.transformarColorANumero(nombreColor);

    this.iluminarColor(nombreColor);
    if (numeroColor === this.secuencia[this.subnivel]) {
      this.subnivel++;
      if (this.subnivel === this.nivel) {
        this.nivel++;
        this.eliminarEventosClick();
        if (this.nivel === (ULTIMO_NIVEL + 1)) {
          detenerContador();
          this.ganaJuego();
        } else {
          setTimeout(this.siguienteNivel, 1500);
        }
      }
    } else {
      detenerContador();
      this.pierdeJuego();
    }
  }

  /*Muestra la alerta que indica que ganamos el juego y al dar ok, llama al tablero de resultados().
    llama a inicializar para que las variables del juego se reinicien y así poder jugar de nuevo
    sin refrescar la pagina */
  ganaJuego() {
    swal("¡WOW!", "¡FELICIDADES! Ganaste el Juego", "success")
      .then(() => {
        this.tableroResultados();
        this.inicializar()
      })
  }

  /*Muestra la alerta que indica que perdimos el juego y al dar ok, llama al tablero de resultados().
  llama a inicializar para que las variables del juego se reinicien y así poder jugar de nuevo
  sin refrescar la pagina */
  pierdeJuego() {
    swal("¡Mala Suerte!", "Lo siento, perdiste la partida :(", "error")
      .then(() => {
        this.tableroResultados();
        this.eliminarEventosClick()
        this.inicializar()
      })
  }

  /*El tablero de resultados nos va a mostrar el puntaje p/ULTIMO_NIVEL, el puntaje viene de la variable nivel,
  tomando en cuenta que dicha variable es inicializada en 1.
  Dependiendo del nivel al que llega el jugador nos va a mostrar un mensaje diferente */
  tableroResultados() {
    var nivelActual = this.nivel - 1;
    var mensaje;
    if (nivelActual < 7) {
      mensaje = "¡¡¡¡¡Vamos no te rindas!!!!!";
    } else if ((nivelActual >= 7) && (nivelActual < 10)) {
      mensaje = "¡¡¡¡¡¡¡¡¡No está mal!!!!!!!!!"
    } else if (nivelActual < ULTIMO_NIVEL) {
      mensaje = "¡¡Tu puedes, casi lo logras!!"
    } else {
      mensaje = "¡¡¡muy bien, lo lograste!!!"
    }

    swal({
      title: "¡RESULTADOS!",
      text: `Puntuación: ${nivelActual}/${ULTIMO_NIVEL}\n Tiempo: ${tiempo.toFixed(2)} seg.\n\n ${mensaje}\n\n\nRepositorio del proyecto: \nhttps://gitlab.com/chichianichi/simon-dice`
    })
  }

}

/*Mostrará las instrucciones del juego desde una alerta */
function instrucciones() {
  
  swal({
    title: "Instrucciones",
    text: `El juego consiste en seguir el patrón que se te va a mostrar, cada nivel extenderá el patrón a seguir. Para poder ganar el juego debes completar ${ULTIMO_NIVEL} niveles, si quieres llegar al final te recomiendo poner atención en los colores y  sonidos.`,
    button: "Entendido"
  })
}

/*Funcion que crea la instancia de la clase Juego */
function empezarJuego() {

  window.juego = new Juego();
}

/*Funcion que inicia el cronometro */
function iniciaContador() {
  if (verificador == false) {
    intervalo = setInterval(function () {
      tiempo += 0.01;
    }, 10);
    verificador = true;
  }
}

/*Funcion que detiene el cronometro */
function detenerContador() {
  if (verificador) {
    verificador = false;
    clearInterval(intervalo);
  }
}
