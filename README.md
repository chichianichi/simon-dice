# Simón Dice

<h3>Proyecto desarrollado en el curso de JavaScript.</h3>

<h5>El proyecto de este juego es desarrollado para la práctica de código con JavaScript.</h5>

<h5>Con el conocimiento que adquirí en el curso, hice ciertas modificaciones sin alterar la esencia del juego y ahora es posible ver el juego en ejecución, previamente se ha configurado un website estático con la ayuda de la herramienta GitLab Pages, en conjunto con GitLab CI y GitLab Runner.</h5>


##### El curso fué impartido por [Platzi.com](https://platzi.com/ "Platzi.com") y tiene el nombre [Fundamentos de JavaScript](https://platzi.com/cursos/fundamentos-javascript/ "Fundamentos de JavaScript")

##### Si te gustaría probar el juego en ejecución has click  [Aquí](https://chichianichi.gitlab.io/simon-dice "Aquí")
